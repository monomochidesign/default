$(document).ready(function(){
   var $items = $('#list');
	$.ajax({
		url:'data/recommendations.json',
		dataType: 'json',
		type: 'get',
		cache: false,
		success: function(data){
			$(data.placements).each(function(index,value){
				//Checking to see the items are being passed through
				//console.log(value.items);
				$('.heading').html(value.message); //assigns the message json value to the heading class.

				//For each item under items, create a list and insert anchor link, image tag and print out the item's name and price.
				$.each(value.items,function(key,item){
					$items.append("<li>" + "<a href=" + item.linkURL + ">" + "<img src=" + item.imageURL + ">" + "</a>" + "<h5>" + item.name + "</h5>" + "<p>" + "&pound;" + item.price + "</p>" + "</li>");	
					$("li").addClass("col-sm-6 col-md-2");
					$("li:first").addClass("col-sm-offset-1");
					$("img").addClass("img-responsive");
					//console.log("success");
				});

			});
		}
	});	
		
});

