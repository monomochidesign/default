In this excercise we would like you to build a web component similar to this in bootstrap 3 [screenshot](recommendation_screenshot.png) using the [JSON](data/recommendations.json) provided. Creativity is accepted but do not alter the JSON. The componet you build should display images, prices and link to the product taking into consideration how it would work with i18n.

### Requirements
* Responsive
* Reusable code

Please fork this repository and commit your changes for review.

Amend this Readme in your forked repo and use your commits to outline the component you have created and the decisions that you have made.

----

Hi,

I've had to alter your JSON file as some of the products are no longer available from the website. The images were also not accessible so i've updated all the image urls in your file.

I've uploaded my files to bitbucket, you can access it using the below info.
https://bitbucket.org/monomochidesign/default

I've used the AJAX method to access your JSON file. 
Console logged to make sure i'm seeing the objects within. 
The main message was called using value.message and displayed it in the div with class "heading". 
For the rest of the items, i've used a for each method to loop through the JSON objects and appended them to the <ul> as list items. Also assigned a few classes to them inorder to create the 5 column layout.
As bootstrap isnt the best at creating 5 column layouts, ive had to offset the first list item and remove the offset when the screen is smaller (otherwise itll create an uneven list). Also assigned a img-responsive class to the images so that it will fit the max width of its container. 